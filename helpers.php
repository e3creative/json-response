<?php

if (!function_exists('json_response')) {
    function json_response($content = '', $status = 200, array $headers = array())
    {
        $factory = app('json-response');

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($content, $status, $headers);
    }
}
