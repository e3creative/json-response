<?php

use Orchestra\Testbench\TestCase;
use E3Creative\JsonResponse\JsonResponse;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Contracts\View\Factory as ViewFactory;

class JsonResponseTest extends TestCase
{
    public function test_it_can_return_success_response()
    {
        $jsonResponse = new JsonResponse(
            app(ViewFactory::class),
            app('redirect')
        );

        $response = $jsonResponse->success('Hooray you did it');

        $expectedData = ['message' => 'Hooray you did it'];
        $actualData = json_decode($response->getContent(), true);

        $this->assertEquals($expectedData, $actualData);
        $this->assertEquals(HttpResponse::HTTP_OK, $response->getStatusCode());
    }

    public function test_it_can_return_empty_response()
    {
        $jsonResponse = new JsonResponse(
            app(ViewFactory::class),
            app('redirect')
        );

        $response = $jsonResponse->empty();

        $actualData = json_decode($response->getContent(), true);

        $this->assertEquals([], $actualData);
        $this->assertEquals(HttpResponse::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function test_it_can_return_created_response()
    {
        $jsonResponse = new JsonResponse(
            app(ViewFactory::class),
            app('redirect')
        );

        $response = $jsonResponse->created(['name' => 'June Jones']);

        $expectedData = ['name' => 'June Jones'];
        $actualData = json_decode($response->getContent(), true);

        $this->assertEquals($expectedData, $actualData);
        $this->assertEquals(HttpResponse::HTTP_CREATED, $response->getStatusCode());
    }

    public function test_it_can_return_error_response()
    {
        $jsonResponse = new JsonResponse(
            app(ViewFactory::class),
            app('redirect')
        );

        $response = $jsonResponse->error(
            'Oh no, it did an error',
            HttpResponse::HTTP_INTERNAL_SERVER_ERROR,
            ['name' => 'The name field is required']
        );

        $expectedData = [
            'message' => 'Oh no, it did an error',
            'errors' => [
                'name' => 'The name field is required',
            ],
            'code' => 5000,
        ];

        $actualData = json_decode($response->getContent(), true);

        $this->assertEquals($expectedData, $actualData);
    }

    public function test_it_can_return_unprocessable_response()
    {
        $jsonResponse = new JsonResponse(
            app(ViewFactory::class),
            app('redirect')
        );

        $response = $jsonResponse->unprocessable([
            'name' => 'The name field is required',
        ]);

        $expectedData = [
            'message' => 'The given data was invalid',
            'errors' => [
                'name' => 'The name field is required',
            ],
            'code' => 4220,
        ];

        $actualData = json_decode($response->getContent(), true);

        $this->assertEquals($expectedData, $actualData);
    }

    /**
     * @dataProvider codesProvider
     */
    public function testItCanReturnCorrectStatusCodesForErrorResponses($method, $args, $expectedStatusCode)
    {
        $jsonResponse = new JsonResponse(
            app(ViewFactory::class),
            app('redirect')
        );

        $actual = $jsonResponse->$method(...$args);

        $this->assertEquals($expectedStatusCode, $actual->getStatusCode());
    }

    public function codesProvider()
    {
        return [
            [
                'unprocessable',
                [['Oh no' => 'it did an error'], 422],
                HttpResponse::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'unprocessable',
                [['Oh no' => 'it did an error'], 4222],
                HttpResponse::HTTP_UNPROCESSABLE_ENTITY
            ],
            [
                'error',
                ['Oh no it did an error', 500],
                HttpResponse::HTTP_INTERNAL_SERVER_ERROR
            ],
            [
                'error',
                ['Oh no it did an error', 5001],
                HttpResponse::HTTP_INTERNAL_SERVER_ERROR
            ],
        ];
    }
}
