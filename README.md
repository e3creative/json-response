# json-response

This package provides a response wrapper that adds functionality for a json api.


## Pulling in with composer:

1. Install the package:
`composer require e3creative/json-response`


## Usage

You can use Laravel's `Response` class as normal with the additional JSON methods this package provides, e.g.

```
Response::empty();
Response::created($data);
Response::success($message, $status);
Response::error($message, $status, $data);
Response::unprocessable($errors);
```

```
public function index(Response $response) {
    return $response->empty();
}
```

If you follow step two of the installation instructions above, you can also use these methods with the `response()` function:

```
json_response()->empty();
json_response()->created($data);
json_response()->success($message, $status);
json_response()->error($message, $status, $data);
json_response()->unprocessable($errors);
```

### Error response codes
The `error()` method expects a four digit response code. This will be transformed into three digit HTTP response code for the request, but the four digit response code will be returned in the response body. This allows for more granular error response identifcation in app/frontends.
