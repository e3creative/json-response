# Changelog

## [3.0.0] - 2019-10-31
### Changed
- Updates vendor name

## [2.0.0] - 2019-06-19
- Changes key returned by error responses to 'errors'
- Adds 'unprocessable' method for validation errors

## [1.0.1] - 2018-10-08
- Fixes implementation of empty and created methods to use the base json method rather than success

## [1.0.0] - 2018-10-08
### Initial files
