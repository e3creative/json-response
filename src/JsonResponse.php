<?php

namespace E3Creative\JsonResponse;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Response as HttpResponse;

class JsonResponse extends ResponseFactory
{
    /**
     * Successful json response.
     *
     * @param $data
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function success($message, $status = HttpResponse::HTTP_OK)
    {
        return $this->json(['message' => $message], $status);
    }

    /**
     * Successful empty json response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function empty()
    {
        return $this->json([], HttpResponse::HTTP_NO_CONTENT);
    }

    /**
     * Successful created json response.
     *
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function created($data)
    {
        return $this->json($data, HttpResponse::HTTP_CREATED);
    }

    /**
     * Error json response. If the code passed is 3 digits, transform it into a
     * four digit one for the actual HTTP code passed in the body. If not, leave
     * as is.
     *
     * @param $message
     * @param int $code
     * @param null $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function error(string $message, $code = 5000, $errors = null)
    {
        $code = strlen($code) === 3 ? $code * 10 : $code;
        $httpCode = intval(floor($code / 10));

        $body = [
            'message' => $message,
            'code' => $code,
        ];

        if ($errors) {
            $body['errors'] = $errors;
        }

        return $this->json($body, $httpCode);
    }

    /**
     * Unprocessable entity json response.
     *
     * @param  array  $errors
     * @param  integer $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function unprocessable(array $errors, $code = 4220)
    {
        return $this->error('The given data was invalid', $code, $errors);
    }
}
