<?php

namespace E3Creative\JsonResponse\Providers;

use E3Creative\JsonResponse\JsonResponse;
use Illuminate\Contracts\View\Factory as ViewFactory;
use E3Creative\JsonResponse\Facades\JsonResponseFacade;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {
        $this->app->bind('json-response', function ($app) {
            return new JsonResponse($app[ViewFactory::class], $app['redirect']);
        });

        require __DIR__ . '/../../helpers.php';
    }
}
