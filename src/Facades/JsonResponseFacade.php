<?php

namespace E3Creative\JsonResponse\Facades;

use Illuminate\Support\Facades\Facade;

class JsonResponseFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'json-response';
    }
}
